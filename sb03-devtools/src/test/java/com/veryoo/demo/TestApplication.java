package com.veryoo.demo;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.veryoo.demo.service.UserService;

@SpringBootTest
public class TestApplication {

	@Autowired
	private UserService userService;
	
	@Test
	public void testUserService() {
		assertEquals(100, userService.countUser());
	}
}
